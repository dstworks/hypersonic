const dataMode = false;
const debugMode = false;
var step = -1;

function canContinue() {
    step++;
    if (!debugMode) {
        return true;
    } else {
        var lackOfData = data.length == 0;
        if (lackOfData) {
            if (!dataMode) {
                printErr("replay data ends, at step " + step);
            }
        }
        var isHeroExploded = exPro.getExplodedCellsForNextTurn(affectedByBombs).filter(a=>a.x == hero.x && a.y == hero.y).length > 0;
        if (isHeroExploded) {
            if (!dataMode) {
                printErr("sorry, hero exploded! at step " + step);
            }
        }
        return !lackOfData && !isHeroExploded;
    }
}

/**
 *
 * @param {Array.<Array.<Node>>} map
 * @constructor
 */
function ExplodeProcessor(map) {
    "use strict";

}

ExplodeProcessor.prototype.map = null;
ExplodeProcessor.prototype.getExplodedCellsForNextTurn = function (affectedByBombs) {
    return affectedByBombs.filter(a=>Math.min(...a.affectedBy.filter(b=>!b.isMy()).map(b=>b.countdown)) == 1);
};
/**
 * @param {number} turn - number of turns after current, where 1 - is next turn
 */
ExplodeProcessor.prototype.getExplodedCellsForTurn = function (affectedByBombs, turn) {
    return affectedByBombs.filter(a=>Math.min(...a.affectedBy.filter(b=>!b.isMy()).map(b=>b.countdown)) == turn);
};
var exPro = new ExplodeProcessor();
/*
 var affected = [
 {x:1,y:1, affectedBy:[{countdown:3,isMy:()=>false},{countdown:52,isMy:()=>true}]},
 {x:2,y:2, affectedBy:[{countdown:6,isMy:()=>true},{countdown:1,isMy:()=>false}]},
 {x:3,y:3, affectedBy:[{countdown:5,isMy:()=>false},{countdown:1,isMy:()=>true}]},
 {x:4,y:4, affectedBy:[{countdown:2,isMy:()=>true},{countdown:525,isMy:()=>false}]}
 ];

 var hero={x:3,y:3};
 var aff = new ExplodeProcessor().getExplodedCellsForNextTurn(affected).filter(a=>a.x==hero.x&&a.y==hero.y);

 alert(aff.length==1&&aff.map(a=>a.x+"x"+a.y))
 */

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

var inputs = readline().split(' ');
var width = parseInt(inputs[0]);
var height = parseInt(inputs[1]);
var myId = parseInt(inputs[2]);
var lastX = -1;
var lastY = -1;
if (dataMode) {
    printErr(inputs.join(" "));
}
/**
 printErr(inputs.join(" "));
 Array(height).fill().map(a=>//printErr(readline()));
 var e=readline();
 printErr(e)
 Array(+e).fill().map(a=>//printErr(readline()));
 */
var entityTypeMap = {0: "player", 1: "bomb", 2: "item"};
var targetTypeMap = {0: "escape", 1: "bomb", 2: "item"};
/**
 * @type {Array.<Array.<Node>>}
 */
var map;
/**
 * @type {Array.<Node>}
 */
var sortedMap;

function parseMap() {
    var y = -1;
    map = Array(height).fill().map(a=>readline().split(""));
    if (dataMode) {
        printErr(map.map(a=>a.join("")).join("\n"));
    }
    map = map
        .map(a=> {
                y++;
                var x = 0;
                return a.map(b=> {
                    return new Node(x++, y, b);
                })
            }
        );
}
function inMap(x, y) {
    return x >= 0 && x < width && y >= 0 && y < height;
}
function findNeighbors(x, y, d) {
    var neighbors = [];
    var i = -d;
    while (++i < d) {
        if (i != 0) {
            if (inMap(x, y + i)) {
                var my = map[y + i][x];
                neighbors.push(my);
            }
            if (inMap(x + i, y)) {
                var mx = map[y][x + i];
                neighbors.push(mx);
            }
        }
    }
    return neighbors;
}
/**
 * correct version of findNeighbors() for explosion (wave stop at first box in given direction)
 * @param x
 * @param y
 * @param d
 * @returns {Array}
 */
function findExplodedNeighbors(x, y, d) {
    var neighbors = [];
    for (var range of [{step: -1, end: -d}, {step: 1, end: d}]) {
        var i = 0;
        while (Math.abs(i += range.step) < Math.abs(range.end)) {
            if (i != 0) {
                if (inMap(x, y + i)) {
                    var my = map[y + i][x];
                    var item = my.item;
                    var bomb = my.bomb;
                    if (my.v != "X") {
                        neighbors.push(my);
                    }
                    if (my.v != "." || item || bomb) {
                        break;
                    }
                }
            }
        }
    }
    for (var range of [{step: -1, end: -d}, {step: 1, end: d}]) {
        var i = 0;
        while (Math.abs(i += range.step) < Math.abs(range.end)) {
            if (i != 0) {
                if (inMap(x + i, y)) {
                    var mx = map[y][x + i];
                    var item = mx.item;
                    var bomb = mx.bomb;
                    if (mx.v != "X") {
                        neighbors.push(mx);
                    }
                    if (mx.v != "." || item || bomb) {
                        break;
                    }
                }
            }
        }
    }
    return neighbors;
}

/**
 * returns number of neighbor cells with value,
 * in dist of fire
 */
function findExplodedValuedNeighbors(x, y, d) {
    return findExplodedNeighbors(x, y, d).filter(a=>a.v != "." && a.v != "X");
}

function calcAllNeighbors(flatMap, d) {
    return flatMap.map(a=> {
        nodesMap[a.x + "x" + a.y] = a;
        if (a.v == ".") {
            var ns = findExplodedValuedNeighbors(a.x, a.y, d);
            a.n = ns.length;
            a.ns = ns;
            a.links = findNeighbors(a.x, a.y, 2).filter(b=>b.v == "." && !b.bomb);
        } else {
            a.n = 0;
        }
        return a;
    });
}
function getDistToHero(x, y) {
    var dx = x - hero.x;
    var dy = y - hero.y;
    return Math.sqrt(dx * dx + dy * dy);
}
/**
 * far MovesToHero calculation
 * @param x
 * @param y
 * @returns {number}
 */
function getMovesToHero(x, y) {
    var dx = x - hero.x;
    var dy = y - hero.y;
    return Math.abs(dx) + Math.abs(dy);
}
function processMap() {
    sortedMap = map.reduce((s, c)=>s.concat(c), s = []);
    sortedMap = calcAllNeighbors(sortedMap, hero.range);
    calcPath();
    sortedMap = sortedMap.sort((a, b)=> {
        var r = b.n - a.n;
        if (r == 0) {
            var distToHeroA = getDistToHero(a.x, a.y);
            var distToHeroB = getDistToHero(b.x, b.y);
            if (distToHeroA == distToHeroB) {
                // calc goods sum
                return (b.ns && b.ns.reduce((s, c)=>s + +c.v, 0)) - (a.ns && a.ns.reduce((s, c)=>s + +c.v, 0));
            }
            return distToHeroA - distToHeroB;
        }
        return r;
    }).filter(a=>map[hero.y][hero.x].pathMap[a.id] != null);
    if (!dataMode) {
        /*
         printErr("2,1: " + findExplodedValuedNeighbors(2, 1, 3).length);
         printErr(sortedMap.map(b=>
         "{x:" + (b.x + "").padEnd(2, " ") +
         ",y:" + (b.y + "").padEnd(2, " ") +
         ",v:" + b.v +
         ",n:" + (b.n + "").padEnd(2, " ") + "}"
         ).filter((a, i)=>i < 5).join(""));
         */
    }
}


function Node(x, y, v) {
    // value
    this.v = v;
    // coords
    this.x = x;
    this.y = y;
    //id
    this.id = x + "x" + y;
    // tree
    this.links = [];
    this.pathes = [];
    this.pathMap = {};
    this.isGateway = false;
}
Node.prototype.nextStepTo = function (id) {
    var node = this.pathMap[id];
    if (!node) {
        return {x: id.split("x")[0], y: id.split("x")[1]};
    }
    while (node.uplink != node.root) {
        node = node.uplink;
    }
    return node;
}
Node.prototype.toString = function () {
    return Object.keys(this).map(a=>a + ":" + this[a]).join(", ");
}
function copyNode(node, uplink, length, root) {
    var n = new Node(node.x, node.y, node.v);
    n.links = node.links.slice();
    n.uplink = uplink;
    n.length = length;
    n.root = root;
    n.orig = node.orig || node;
    n.v = node.v;
    n.isGateway = node.isGateway;
    return n;
}
function calcPath() {
    var gateway = map[hero.y][hero.x];
    gateway.pathes = [];
    gateway.pathMap = {};
    gateway.pathes.push(copyNode(gateway, gateway, 0, gateway));
    gateway.pathMap[gateway.id] = gateway;
    var length = 1;
    var currentArray = gateway.links.map(a=>copyNode(a, gateway, length, gateway));
    while (gateway.links.length != 0 && currentArray.length > 0) {
        length++;
        var nextArray = [];
        while (currentArray.length > 0) {
            var node = currentArray.pop();
            if (!gateway.pathMap[node.id]) {
                if (!node.isGateway) {
                    nextArray = nextArray.concat(node.links.map(a=>copyNode(a, node, length, gateway)));
                } else {
                    //printErr("omit gateway links: - "+node.id);
                }
                gateway.pathes.push(node);
                gateway.pathMap[node.id] = node;
            }
        }
        currentArray = nextArray;
    }
    gateway.pathes.sort((a, b)=>a.id - b.id);
    gateway.pathes.map(a=>"id: " + a.id + ", v: " + a.v + ", length: " + a.length);
}

/**
 * Base entity
 * @param {number} entityType
 * @param {number} owner
 * @param {number} x
 * @param {number} y
 * @param {number} param1
 * @param {number} param2
 * @constructor
 */
function Entity(entityType, owner, x, y, param1, param2) {
    this.type = entityType;
    this.owner = owner;
    this.x = x;
    this.y = y;
    this.param1 = param1;
    this.param2 = param2;
}
Entity.prototype.type = null;
Entity.prototype.owner = null;
Entity.prototype.x = null;
Entity.prototype.y = null;
Entity.prototype.param1 = null;
Entity.prototype.param2 = null;
Entity.prototype.isMy = function () {
    return this.owner == myId;
};

/**
 *
 * @param {Entity} entity
 * @constructor
 */
function Player(entity) {
    this.id = entity.owner;
    this.x = entity.x;
    this.y = entity.y;
    this.bombs = entity.param1;
    this.range = entity.param2;
    if (!dataMode) {
        //printErr("player " + (this.isMy() ? "me" : this.id) + ": " + hero.x + "x" + hero.y + ": " + map[this.y][this.x]);
    }
    this.node = map[this.y][this.x];
}
Player.entityType = 0;
Player.prototype.id = null;
Player.prototype.x = null;
Player.prototype.y = null;
Player.prototype.bombs = null;
Player.prototype.range = null;
Player.prototype.isMy = function () {
    return this.id == myId;
};
/**
 *
 * @param {Entity} entity
 * @constructor
 */
function Bomb(entity) {
    this.owner = entity.owner;
    this.x = entity.x;
    this.y = entity.y;
    this.countdown = entity.param1;
    this.range = entity.param2;
}
Bomb.entityType = 1;
Bomb.prototype.owner = null;
Bomb.prototype.x = null;
Bomb.prototype.y = null;
Bomb.prototype.countdown = null;
Bomb.prototype.range = null;
Bomb.prototype.isMy = function () {
    return this.owner == myId;
};
/**
 *
 * @param {Entity} entity
 * @constructor
 */
function Item(entity) {
    this.x = entity.x;
    this.y = entity.y;
    this.type = entity.param1;
}
Item.entityType = 2;
Item.prototype.x = null;
Item.prototype.y = null;
Item.prototype.type = null;

var activeTargetX = -1;
var activeTargetY = -1;
var hero = {range: 3};
/**
 * hero targets on map with type (for bomb, to escape, for item)
 * @type {Array}
 */
var targets = [];
/**
 * players
 * @type {Array.<Player>}
 */
var players = [];
/**
 * bombs
 * @type {Array.<Bomb>}
 */
var bombs = [];
var affectedByBombs = [];
/**
 * items
 * @type {Array.<Item>}
 */
var items = [];
/**
 * items
 * @type {Array.<Entity>}
 */
var entities = [];
var nodesMap = {};

function resetCollections() {
    entities = [];
    players = [];
    bombs = [];
    items = [];
    affectedByBombs = [];
    nodesMap = {};
}

/**
 * parse entities and set my player data
 */
function parseEntities() {
    var entitiesCount = parseInt(readline());
    if (dataMode) {
        printErr(entitiesCount);
    }
    for (var i = 0; i < entitiesCount; i++) {
        var inputs = readline().split(' ');
        if (dataMode) {
            printErr(inputs.join(" "));
        }
        var entityType = parseInt(inputs[0]);
        var owner = parseInt(inputs[1]);
        var x = parseInt(inputs[2]);
        var y = parseInt(inputs[3]);
        var param1 = parseInt(inputs[4]);
        var param2 = parseInt(inputs[5]);
        var entity = new Entity(entityType, owner, x, y, param1, param2);
        entities.push(entity);
        //printErr((entityTypeMap[entityType] || entityType) + " (" + (owner == 0 ? "me" : owner) + "): " + x + ' ' + y + ", params: " + param1 + ", " + param2);
    }

    // preprocess
    for (var entity of entities) {
        // it is player
        if (entity.type == Player.entityType) {
            var player = new Player(entity);
            if (player.isMy()) {
                hero = player;
            }
            players.push(player);
        }

        // it is a bomb
        if (entity.type == Bomb.entityType) {
            var bomb = new Bomb(entity);
            map[entity.y][entity.x].bomb = bomb;
            bombs.push(bomb);
        }

        // it is an item
        if (entity.type == Item.entityType) {
            var item = new Item(entity);
            map[entity.y][entity.x].item = item;
            items.push(item);
        }
    }
}

/**
 * process entities
 */
function processEntities() {
    // process bombs
    for (var bomb of bombs) {
        // mark all cells that will be affected by bomb
        if (!dataMode) {
            //printErr("marking cells for bomb: " + bomb.owner + "," + bomb.countdown + "," + bomb.range);
        }
        var ens = findExplodedNeighbors(bomb.x, bomb.y, bomb.range);
        bomb.affectedBombs = ens.map(a=>a.bomb).filter(a=>a);
        /*for (var addBomb of bomb.affectedBombs) {
         addBomb.countdown = Math.min(addBomb.countdown, bomb.countdown);
         }*/
        affectedByBombs.push(...ens
            .map(a=> {
                // TODO: check explode range stops at wall/box/bomb
                // mark cell what bomb will affect it
                (a.affectedBy = (a.affectedBy || [])).push(bomb);
                if (a.affectedAfter == null) {
                    a.affectedAfter = bomb.countdown;
                }
                a.affectedAfter = Math.min(a.affectedAfter, bomb.countdown);
                if (!dataMode) {
                    //printErr("(" + a.x + "," + a.y + ").affectedAfter: " + a.affectedAfter);
                }
                return a;
            }));
    }
    for (var bomb of bombs) {
        var bs = [];
        if (bomb.affectedBombs.length > 0) {
            bs.push(bomb);
            bs.push(...bomb.affectedBombs);
            var minCountdown = bs.reduce((min, c)=>min > c.countdown ? c.countdown : min, Number.MAX_SAFE_INTEGER);
            if (!dataMode) {
                printErr("bs number: " + bs.length + ": " +
                    bs.map(a=>a.countdown).join(", ") + ": countdown to " + minCountdown
                )
            }
            bs.forEach(b=>b.countdown = minCountdown);
        }
    }

    // remove resources that will be affected by bomb
    affectedByBombs.map(a=> {
        a.v = ".";
        return a;
    });
}

// game loop
while (canContinue()) {
    resetCollections();

    parseMap();

    parseEntities();

    // calc neighbors
    processMap();

    // create objects, calc explodes etc
    processEntities();

    // init target
    if (activeTargetY == -1) {
        activeTargetX = sortedMap[0].x;
        activeTargetY = sortedMap[0].y;
        targets.unshift({x: activeTargetX, y: activeTargetY, type: Bomb.entityType});
        if (!dataMode) {
            //printErr("target init finished");
        }
    }

    // default action and coords
    var action = "MOVE";

    var x = hero.x;
    var y = hero.y;

    if (!dataMode) {
        printErr("Moving from " + x + "," + y + " to " + activeTargetX + "," + activeTargetY);
    }

    if (x == activeTargetX && y == activeTargetY) {
        if (targets[0].type == 0) {
            // safe place reached
            // free stack
            targets.shift();
            // if no tasks move to nearest best-bomb-place
            if (targets.length == 0) {
                activeTargetX = sortedMap[0].x;
                activeTargetY = sortedMap[0].y;
                targets.unshift({x: activeTargetX, y: activeTargetY, type: Bomb.entityType});
            } else {
                // or continue tasks
                activeTargetX = targets[0].x;
                activeTargetY = targets[0].y;
            }
            // redefine output
            action = "MOVE";
        } else if (targets[0].type == Item.entityType) {
            // item found
            targets.shift();
            if (!dataMode) {
                printErr("item found, continue")
            }
            // if no tasks move to nearest best-bomb-place
            if (targets.length == 0) {
                activeTargetX = sortedMap[0].x;
                activeTargetY = sortedMap[0].y;
                targets.unshift({x: activeTargetX, y: activeTargetY, type: Bomb.entityType});
            } else {
                // or continue tasks
                activeTargetX = targets[0].x;
                activeTargetY = targets[0].y;
            }
            // redefine output
            action = "MOVE";
        } else if (targets[0].type == Bomb.entityType) {
            if (hero.bombs > 0) {
                // free stack
                targets.shift();
                if (targets.length == 0) {
                    // remove resources that will be affected by my bomb
                    var bomb = new Bomb(new Entity(Bomb.entityType, myId, x, y, 9, hero.range));
                    if (!dataMode) {
                        //printErr("marking cells for my bomb: " + bomb.owner + "," + bomb.countdown + "," + bomb.range);
                    }
                    findExplodedValuedNeighbors(x, y, hero.range).map(a=> {
                        a.v = ".";
                        (a.affectedBy = (a.affectedBy || [])).push(bomb);
                        if (a.affectedAfter == null) {
                            a.affectedAfter = bomb.countdown;
                        }
                        a.affectedAfter = Math.min(a.affectedAfter, bomb.countdown);
                        if (!dataMode) {
                            //printErr("(" + a.x + "," + a.y + ").affectedAfter: " + a.affectedAfter);
                        }
                    });

                    // TODO: check - find place for bomb assuming that nearest boxes not yet destroyed!
                    // recalc map
                    processMap();

                    // search closest best-bomb-cell that will be free at arriving time
                    var i = 0;
                    while (i < sortedMap.length) {
                        var moves = getMovesToHero(sortedMap[i].x, sortedMap[i].y);
                        var freeAfter = (sortedMap[i].affectedAfter || 0);
                        if (!dataMode) {
                            try {
                                printErr("calc next (" + i + "?) bomb! (" + sortedMap[i].x + "," + sortedMap[i].y + ") freeAfter: " + freeAfter + ", moves: " + moves);
                            } catch (err) {
                                printErr(err);
                            }
                        }
                        if (freeAfter < moves) {
                            break;
                        }
                        i++;
                    }
                    if (!dataMode) {
                        try {
                            printErr("variant " + i + " selected! (" + sortedMap[i].x + "," + sortedMap[i].y + ") freeAfter: " + freeAfter + ", moves: " + moves);
                        } catch (err) {
                            printErr(err);
                        }
                    }

                    if (i < sortedMap.length) {
                        activeTargetX = sortedMap[i].x;
                        activeTargetY = sortedMap[i].y;
                    }
                    targets.unshift({x: activeTargetX, y: activeTargetY, type: Bomb.entityType});
                } else {
                    activeTargetX = targets[0].x;
                    activeTargetY = targets[0].y;
                }
                // redefine output
                action = "BOMB";

            } else {
                var affectedHeroCell = affectedByBombs.find(a=>a.x == x && a.y == y && a.affectedBy.some(b=>!b.isMy()));
                if (affectedHeroCell) {
                    // calc time to first explosion
                    var timeToExplosion = affectedHeroCell.affectedBy.reduce((s, c)=>c.countdown < s ? c.countdown : s, Number.MAX_SAFE_INTEGER);
                    // look items around
                    // find closest safe item
                    var itemPoint = items
                        .filter(a=>
                            getDistToHero(a.x, a.y) < timeToExplosion * 1.2
                            && !affectedByBombs.some(b=>b.x == a.x && b.y == a.y
                            && !a.bomb)
                        )
                        // and sort by distance to hero, to get closest
                        .sort((a, b)=>getDistToHero(a.x, a.y) - getDistToHero(b.x, b.y))[0];
                    if (timeToExplosion > 5 && itemPoint) {
                        if (!dataMode) {
                            printErr("explosion expected, moving to safe point with item!");
                        }
                        // move to safe point
                        //printErr("move to safe point");
                        targets.unshift({x: itemPoint.x, y: itemPoint.y, type: Item.entityType});
                        activeTargetX = targets[0].x;
                        activeTargetY = targets[0].y;
                        action = "MOVE";
                    }
                    // look safe place around
                    var explodesAffectingHero = true;
                    if (explodesAffectingHero && timeToExplosion < 6) {
                        // find safe point
                        var point = map.reduce((s, c)=>s.concat(c), s = [])
                            .filter(a=>a.v == "." && !affectedByBombs.some(b=>b.x == a.x && b.y == a.y)
                            && !a.bomb)
                            // and sort by distance to hero, to get closest
                            .sort((a, b)=>getDistToHero(a.x, a.y) - getDistToHero(b.x, b.y))[0];

                        // move to safe point
                        if (!dataMode) {
                            printErr("explosion expected, move to safe point!");
                        }
                        targets.unshift({x: point.x, y: point.y, type: 0});
                        activeTargetX = targets[0].x;
                        activeTargetY = targets[0].y;

                        action = "MOVE";
                    }
                }
            }
        }
    } else if (targets[0].type == Bomb.entityType) {
        // try collect item by the way
        var itemCell = items
            .filter(a=>getDistToHero(a.x, a.y) < 6 && (a.x == hero.x || a.y == hero.y))
            // and sort by distance to hero, to get closest
            .sort((a, b)=>getDistToHero(a.x, a.y) - getDistToHero(b.x, b.y))[0];
        if (itemCell) {
            // move to item
            targets.unshift({x: itemCell.x, y: itemCell.y, type: Item.entityType});
            activeTargetX = targets[0].x;
            activeTargetY = targets[0].y;
        } else {
            // if can't move forward, try spawn the bomb
            if (lastX == hero.x && lastY == hero.y) {
                var i = 0;
                while (i < sortedMap.length) {
                    var moves = getMovesToHero(sortedMap[i].x, sortedMap[i].y);
                    var freeAfter = (sortedMap[i].affectedAfter || 0);
                    if (!dataMode) {
                        try {
                            printErr("calc next (" + i + "?) bomb! (" + sortedMap[i].x + "," + sortedMap[i].y + ") freeAfter: " + freeAfter + ", moves: " + moves);
                        } catch (err) {
                            printErr(err);
                        }
                    }
                    if (freeAfter < moves) {
                        break;
                    }
                    i++;
                }
                if (i < sortedMap.length) {
                    activeTargetX = sortedMap[i].x;
                    activeTargetY = sortedMap[i].y;
                } else {
                    var ps = hero.node.links.filter(a=>a.v == "." && !a.bomb);
                    if (ps.length > 0) {
                        activeTargetX = ps[0].x
                        activeTargetY = ps[0].y
                    } else {
                        activeTargetX = hero.x;
                        activeTargetY = hero.y;
                    }
                }
                action = "BOMB";
            }
        }
        if (findExplodedValuedNeighbors(x, y, hero.range).length > 0 && (hero.bombs > 1 || getMovesToHero(activeTargetX, activeTargetY) > 5)) {
            action = "BOMB";
        }
    }
    var tn = hero.node.nextStepTo(activeTargetX + "x" + activeTargetY);
    //safe moving
    var affectedHeroCell = affectedByBombs.find(a=>a.x == tn.x && a.y == tn.y && a.affectedBy.some(b=>!b.isMy()));
    if (affectedHeroCell) {
        // calc time to first explosion
        var timeToExplosion = affectedHeroCell.affectedBy.reduce((s, c)=>c.countdown < s ? c.countdown : s, Number.MAX_SAFE_INTEGER);
        // look items around
        // find closest safe item
        var itemPoint = items
            .filter(a=>
                getDistToHero(a.x, a.y) < timeToExplosion * .5
                && !affectedByBombs.some(b=>b.x == a.x && b.y == a.y)
                && !a.bomb
            )
            // and sort by distance to hero, to get closest
            .sort((a, b)=>getDistToHero(a.x, a.y) - getDistToHero(b.x, b.y))[0];
        /*if (timeToExplosion > 1 && itemPoint) {
         if (!dataMode) {
         printErr("explosion expected, moving to safe point with item!");
         }
         // move to safe point
         //printErr("move to safe point");
         targets.unshift({x: itemPoint.x, y: itemPoint.y, type: Item.entityType});
         activeTargetX = targets[0].x;
         activeTargetY = targets[0].y;
         action = "MOVE";
         }*/
        // look safe place around
        var explodesAffectingHero = true;
        if (explodesAffectingHero && timeToExplosion < 2) {
            // find safe point
            var points = map.reduce((s, c)=>s.concat(c), s = [])
                .filter(a=>!a.bomb && a.v == "." && !affectedByBombs.some(b=>b.x == a.x && b.y == a.y))
                // and sort by distance to hero, to get closest
                .sort((a, b)=>getDistToHero(a.x, a.y) - getDistToHero(b.x, b.y));


            var point = points[0];
            // move to safe point
            if (!dataMode) {
                printErr("safe: " + points.map(a=>a.id).join(", "));
                printErr("explosion expected, move to safe point!");
            }
            targets.unshift({x: point.x, y: point.y, type: 0});
            activeTargetX = targets[0].x;
            activeTargetY = targets[0].y;

            // protect using bomb!!!
            if (hero.bombs > 0) {
                if (!dataMode) {
                    printErr("protect using bomb!!!");
                }
                // it should be a shield
                action = "BOMB";
            } else {
                action = "MOVE";
            }
            tn = hero.node.nextStepTo(activeTargetX + "x" + activeTargetY);
        }
    }
    // safe moving end
    for (var i = 1; i < 3; i++) {
        if (exPro.getExplodedCellsForTurn(affectedByBombs, i).filter(a=>a.x == tn.x && a.y == tn.y).length > 0) {
            if (exPro.getExplodedCellsForTurn(affectedByBombs, i).filter(a=>a.x == hero.x && a.y == hero.y).length == 0) {
                tn.x = hero.x;
                tn.y = hero.y;
                if (!dataMode) {
                    printErr("wait to avoid explosion");
                }
                break;
            }
        }
    }
    print(action + " " + tn.x + " " + tn.y + " " + targetTypeMap[targets[0].type]);

    lastX = hero.x;
    lastY = hero.y;
}